## Ghazwa E Khandaq In Urdu Pdf 63

 
  
 
**## Links to download files:
[Link 1](https://bytlly.com/2tCnmz)

[Link 2](https://geags.com/2tCnmz)

[Link 3](https://urllie.com/2tCnmz)

**

 
 
 
 
 
# Ghazwa e Khandaq in Urdu PDF 63: The Battle of the Trench and its Miracles
 
Ghazwa e Khandaq (also known as Ghazwa e Ahzab or the Battle of the Trench) was one of the most important battles in the early history of Islam. It took place in the year 5 AH (627 CE) when a confederation of Arab and Jewish tribes besieged the city of Medina, where the Prophet Muhammad (peace be upon him) and his followers had migrated. The Muslims defended themselves by digging a trench around the city, following the advice of Salman al-Farsi (may Allah be pleased with him), a Persian companion who had seen this strategy used in his homeland. The siege lasted for 27 days, during which the Muslims faced many hardships and challenges, but also witnessed many miracles and signs of Allah's help and support.
 
In this article, we will explore some of the details and lessons of this remarkable event, based on authentic sources and historical accounts. We will also provide a link to download a PDF file in Urdu language that contains 63 pages of information and analysis about Ghazwa e Khandaq.
 
## The Causes of Ghazwa e Khandaq
 
The main cause of Ghazwa e Khandaq was the hostility and enmity of the Quraysh tribe of Mecca, who had been fighting against the Prophet (peace be upon him) and his followers since they proclaimed their faith in Allah and His Messenger. The Quraysh had suffered a humiliating defeat at the Battle of Badr in 2 AH (624 CE), where they lost many of their leaders and warriors. They tried to avenge their loss at the Battle of Uhud in 3 AH (625 CE), but they failed to wipe out the Muslims, despite inflicting heavy casualties on them.
 
The Quraysh realized that they could not defeat the Muslims alone, so they decided to form an alliance with other tribes who were also opposed to Islam. They contacted Banu Nadir, a Jewish tribe that had been expelled from Medina by the Prophet (peace be upon him) for breaking their treaty and plotting against him. Banu Nadir agreed to join the Quraysh and promised to pay them half of their annual date harvest from Khaybar, a fertile oasis where they had settled after leaving Medina. They also sent emissaries to other Arab tribes, such as Banu Ghatafan, Banu Asad, Banu Sulaym, and others, offering them incentives and inciting them against the Muslims.
 
Thus, a confederation of about 10,000 men was formed under the leadership of Abu Sufyan, the chief of Quraysh. They marched towards Medina with the intention of destroying Islam and its followers once and for all.
 
## The Preparation of Ghazwa e Khandaq
 
The Prophet (peace be upon him) was informed about the impending attack by Allah through revelation. He consulted with his companions (may Allah be pleased with them) about how to deal with this huge threat. Salman al-Farsi (may Allah be pleased with him) suggested that they should dig a trench around Medina to prevent the enemy from entering the city. He said that this was a common practice in Persia when facing a large army. The Prophet (peace be upon him) approved this idea and ordered his companions to start digging immediately.
 
The Muslims divided themselves into groups of ten men each and were assigned different sections of the trench to dig. The Prophet (peace be upon him) himself participated in this work, carrying dirt and stones with his own hands. The trench was about five kilometers long, three meters wide, and two meters deep. It took six days to complete it.
 
The Muslims faced many difficulties and hardships while digging the trench. They had to work day and night without proper food or rest. They suffered from hunger, thirst, cold, fatigue, and fear. Some of them tied stones around their stomachs to ease their hunger pangs. The Prophet (peace be upon him) also did the same thing. He encouraged his companions with words of hope and faith, reminding them of Allah's reward and assistance.
 
## The Miracles of Ghazwa e Kh dde7e20689




